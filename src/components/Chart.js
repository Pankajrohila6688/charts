import React from 'react';
import './Chart.css';
import ChartBox from './ChartBox';
import ChartElement from './ChartElement';

function Chart() {
    return (
        <div className="chart">
            <p>random text</p>
            
            <div className="chart__container">
                <div className="chart__containerLeft">
                    <ChartBox />
                </div>
                <div className="chart__containerRight">
                    <ChartBox />
                </div>
            </div>
            
            <div style={{ display: 'flex' }}>
                <div style={{ flex: 0.5 }}>
                    <ChartElement color1='#ACB9FF' color2="#503795" />
                </div>
                <div style={{ flex: 0.5 }}>
                    <ChartElement color1="#BCFFF8" color2="#19B6A4" />
                </div>
            </div>


        </div>
    )
}

export default Chart;
